package com.example.virtualstore.ui.ViewFragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.Choreographer;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.virtualstore.R;
import com.example.virtualstore.controller.LoadDialog;
import com.example.virtualstore.controller.Tracker;
import com.example.virtualstore.ui.ViewActivity.MainActivity;
import com.example.virtualstore.databinding.FragmentWebBinding;

public class WebFragment extends Fragment {

    private FragmentWebBinding webBinding;
    private MainActivity main;
    private String header;

    public static LoadDialog loadDialog;

//    private ProgressBar progressBar = new ProgressBar(getContext());


    public WebFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        webBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_web, container, false);

        components();

        return webBinding.getRoot();
    }

    /**
     * Metodo para inicializar os componentes
     */
    public void components() {
        main = ((MainActivity) getActivity());
        backNavigationWeb();
        loadDialog = new LoadDialog(getContext());
        webViewClient();
        onBackPressedFragment();
    }

    /**
     * Metodo para abrir o link e navegar dentro da app
     *
     * @return
     */
    @SuppressLint("SetJavaScriptEnabled")
    public void webViewClient() {
        String url = getArguments().getString("key_url");
        webBinding.webView.setWebViewClient(new WebViewClient());
        loadDialog.dismissIt();
        webBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.i("INFOR", "CARREGANDO A PAGINA WEB");
                LoadDialog.showStandardLoading(loadDialog, "Aguarde!");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d("SUCESSO", "PAGINA CARREGADA " + view.getOriginalUrl());
                loadDialog.dismissIt();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
//                Log.e("ERROR", "ERRO AO CARREGAR PAGINA WEB " + error.getErrorCode());
                loadDialog.dismissIt();
            }
        });
        loadDialog.dismissIt();
        //Habilitando o JavaScript
        webBinding.webView.getSettings().setJavaScriptEnabled(true);
        webBinding.webView.getSettings().setUseWideViewPort(true);
        webBinding.webView.getSettings().setLoadWithOverviewMode(true);
        webBinding.webView.getSettings().setSupportZoom(true);
        webBinding.webView.getSettings().setBuiltInZoomControls(true);
        webBinding.webView.getSettings().setDisplayZoomControls(false);
        webBinding.webView.loadUrl(url);

    }

    /**
     * Metodo para passar a url para os fragments
     *
     * @param url
     * @return
     */
    public static WebFragment newInstance(String url) {
        WebFragment fragment = new WebFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key_url", url);
        fragment.setArguments(bundle);
        return fragment;
    }

    /*Metodo para o backSpace do celular*/
    private void onBackPressedFragment() {
        if (webBinding.webView.copyBackForwardList().getCurrentIndex() > 0) {
            webBinding.webView.goBack();
        } else {
            webBinding.getRoot().setFocusableInTouchMode(true);
            webBinding.getRoot().requestFocus();
            webBinding.getRoot().setOnKeyListener((view, i, keyEvent) -> {
                if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    goToCardHome();
                    return true;
                }
                return false;
            });
        }
    }

    /*Metodo para voltar*/
    private void goToCardHome() {
        Fragment fragment = new StoreWebFragment();
        getFragmentManager().popBackStack();
        Navigation.findNavController(getView()).popBackStack();
        MainActivity.navigationView.setVisibility(View.GONE);
    }

    /**
     * Metodo para navigar na web ao clicar no onBackPressed
     */
    public void backNavigationWeb() {
        webBinding.webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }
                return false;
            }
        });
    }
}