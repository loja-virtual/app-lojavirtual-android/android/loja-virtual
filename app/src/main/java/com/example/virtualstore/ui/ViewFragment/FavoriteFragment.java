package com.example.virtualstore.ui.ViewFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualstore.R;
import com.example.virtualstore.adapter.FavoriteAdapter;
import com.example.virtualstore.controller.LoadDialog;
import com.example.virtualstore.databinding.FragmentFavoriteBinding;
import com.example.virtualstore.services.DataBase.ServiceCompanyDataSource;
import com.example.virtualstore.services.model.Company;
import com.example.virtualstore.services.model.StoreWeb;
import com.example.virtualstore.ui.ViewActivity.MainActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;


public class FavoriteFragment extends Fragment implements View.OnClickListener {

    private FragmentFavoriteBinding favoriteBinding;
    private MainActivity main;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<StoreWeb> favoriteListWeb = new ArrayList<>();

    private ServiceCompanyDataSource mServiceCompanyDataSource;

    private LoadDialog loadDialogServiceFav;

    private SharedPreferences favorites;
    private SharedPreferences.Editor favEditor;

    public FavoriteFragment() {
        // Required empty public constructor
    }


    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        favorites = getContext().getSharedPreferences(getContext().getString(R.string.preference_favorites), Context.MODE_PRIVATE);
        favEditor = favorites.edit();

        favoriteBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false);

        components();

        return favoriteBinding.getRoot();
    }

    /**
     * Metodo para inicializar os componentes
     */
    public void components() {
        main = ((MainActivity) getActivity());
        loadDialogServiceFav = new LoadDialog(getContext());
        favoriteBinding.btnBackHomeFav.setOnClickListener(this);
        setmRecyclerView();
        onBackPressedFragment();
        MainActivity.navigationView.setVisibility(View.GONE);
    }

    /**
     * Metodo para click
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_backHomeFav) {
            goToHome();
        }
    }

    /**
     * Metodo para iniciar as API's
     */
    public void setAPI() {
        mServiceCompanyDataSource = ServiceCompanyDataSource.getInstance();
    }

    /**
     * Metodo para setar o RecyclerView
     */
    public void setmRecyclerView() {
        favoriteBinding.layoutListCompanyFav.setVisibility(View.GONE);
        favoriteBinding.layoutNoCompanyFav.setVisibility(View.VISIBLE);

        Gson gson = new Gson();

        favoriteBinding.recyclerFav.setHasFixedSize(true);
        int numberOfColumns = 2;

        mLayoutManager = new GridLayoutManager(getContext(), numberOfColumns);
        favoriteListWeb.clear();

        for (Object entry : favorites.getAll().values()) {
            favoriteListWeb.add(gson.fromJson((String) entry, StoreWeb.class));
            favoriteBinding.layoutListCompanyFav.setVisibility(View.VISIBLE);
            favoriteBinding.layoutNoCompanyFav.setVisibility(View.GONE);
        }

        mAdapter = new FavoriteAdapter(getContext(), favoriteListWeb, new FavoriteAdapter.OnClick() {
            @Override
            public void selectFavStore(StoreWeb storeWeb, int position) {
                Log.d("Click on StoreWeb", storeWeb.toString());
                favoriteListWeb.clear();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, WebFragment.newInstance(storeWeb.getCompany().getLink()))
                        .addToBackStack(null)
                        .commit();
                MainActivity.navigationView.setVisibility(View.GONE);
            }
        });

        favoriteBinding.recyclerFav.setLayoutManager(mLayoutManager);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animationController = AnimationUtils.loadLayoutAnimation(getContext(), resId);
        favoriteBinding.recyclerFav.setLayoutAnimation(animationController);

        favoriteBinding.recyclerFav.setAdapter(mAdapter);
    }

    /**
     * Metodo para instanciar o fragmento
     *
     * @return
     */
    public static FavoriteFragment newInstance() {
        return new FavoriteFragment();
    }

    /*Metodo para o backSpace do celular*/
    private void onBackPressedFragment() {
        favoriteBinding.getRoot().setFocusableInTouchMode(true);
        favoriteBinding.getRoot().requestFocus();
        favoriteBinding.getRoot().setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                goToHome();
                return true;
            }
            return false;
        });
    }

    /*Metodo para voltar*/
    private void goToHome() {
        Fragment fragment = new MainFragment();
        main.openFragment(fragment);
        MainActivity.navigationView.setVisibility(View.VISIBLE);
        MainActivity.navigationView.getMenu().getItem(0).setChecked(true);
    }
}