package com.example.virtualstore.ui.ViewActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Explode;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.virtualstore.BuildConfig;
import com.example.virtualstore.R;
import com.example.virtualstore.databinding.ActivitySplashBinding;

public class SplashActivity extends AppCompatActivity {

    private ActivitySplashBinding splashBinding;

    private Animation animation;
    private Animation animText;

    private static final int SPLASH_DELAY = 7000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        adjustFontScale(getResources().getConfiguration());
        splashBinding = DataBindingUtil.setContentView(this,R.layout.activity_splash);

        components();

    }

    public void components(){
        splashBinding.version.setText(BuildConfig.VERSION_NAME);
        splash();
        animTextTouch();
        animTextPay();
    }

    /**
     * Metodo para SplashScreen
     */
    public void splash(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.flipper_in_animation, R.anim.flipper_out_animation);
                finish();
            }
        },SPLASH_DELAY);
    }

    /**
     * Metodo para animar imagem na tela
     */
    public void animImage() {
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.flip_animation);
        //splashBinding.imageSplash.startAnimation(animation);
    }

    /**
     * Metodo para animar texto na tela
     */
    public void animTextTouch() {
        animText = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_mleft_to_center);
        splashBinding.textTouch.startAnimation(animText);
    }

    /**
     * Metodo para animar texto na tela
     */
    public void animTextPay() {
        animText = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right_to_center);
        splashBinding.textPay.startAnimation(animText);
    }

    /**
     * Metoddo implementa o hasFocus
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    /**
     * Metodo para esconder os botoes nativos do android
     */
    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE

                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    /**
     * Metodo para Ajustar o size do dispositivo
     */
    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }
}