package com.example.virtualstore.ui.ViewFragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.virtualstore.BuildConfig;
import com.example.virtualstore.R;
import com.example.virtualstore.databinding.FragmentMenuBinding;
import com.example.virtualstore.databinding.FragmentMenuOnBinding;
import com.example.virtualstore.ui.ViewActivity.MainActivity;
import com.google.android.material.dialog.MaterialDialogs;

import me.drakeet.materialdialog.MaterialDialog;

public class MenuFragment extends Fragment implements View.OnClickListener {

    private FragmentMenuBinding menuBinding;
    private MainActivity main;
    private MaterialDialog materialDialog;


    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        menuBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);

        components();

        return menuBinding.getRoot();
    }

    /**
     * Metodo para inicializar os componentes
     */
    @SuppressLint("RestrictedApi")
    public void components() {
        main = ((MainActivity) getActivity());
        menuBinding.textVersion.setText(BuildConfig.VERSION_NAME);
        MainActivity.navigationView.setVisibility(View.GONE);
        menuBinding.layoutAssessment.setOnClickListener(this);
        menuBinding.layoutApp.setOnClickListener(this);
        menuBinding.layoutVersion.setOnClickListener(this);
        onBackPressedFragment();
    }

    /**
     * Metodo para Click
     */
    @SuppressLint({"NonConstantResourceId", "ShowToast"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layoutAssessment:
                Intent intent;
                try {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.softtechnology.virtualstore"));
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException e) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.softtechnology.virtualstore"));
                    startActivity(intent);
                }
                break;

            case R.id.layoutApp:
                try {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://play.google.com/store/apps/developer?id=Soft-Technology"));
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException e) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Soft-Technology"));
                    startActivity(intent);
                }
                break;

            case R.id.layoutVersion:
                Toast.makeText(getContext(), R.string.you_are_using_the_latest_version, Toast.LENGTH_SHORT);
                break;
        }
    }
    
    /**
     * Metodo para instanciar o fragmento
     *
     * @return
     */
    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    /*Metodo para o backSpace do celular*/
    private void onBackPressedFragment() {
        menuBinding.getRoot().setFocusableInTouchMode(true);
        menuBinding.getRoot().requestFocus();
        menuBinding.getRoot().setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                goToHome();
                return true;
            }
            return false;
        });
    }

    /*Metodo para voltar*/
    private void goToHome() {
//        Fragment fragment = new MainFragment();
//        main.openFragment(fragment);
        Navigation.findNavController(getView()).popBackStack();
        MainActivity.navigationView.setVisibility(View.VISIBLE);
        MainActivity.navigationView.getMenu().getItem(0).setChecked(true);

    }
}