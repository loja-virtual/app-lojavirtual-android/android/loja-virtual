package com.example.virtualstore.ui.ViewActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.example.virtualstore.BuildConfig;
import com.example.virtualstore.R;
import com.example.virtualstore.controller.ExceptionHandlerApp;
import com.example.virtualstore.controller.Permission;
import com.example.virtualstore.databinding.ActivityMainBinding;
import com.example.virtualstore.ui.ViewFragment.FavoriteFragment;
import com.example.virtualstore.ui.ViewFragment.MainFragment;
import com.example.virtualstore.ui.ViewFragment.MenuOnFragment;
import com.example.virtualstore.ui.ViewFragment.WebFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ActivityMainBinding mainBinding;
    private WebFragment webFragment;

    public static BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        adjustFontScale(getResources().getConfiguration());
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerApp(this,MainActivity.class));
        appCenter();
        favorites();
        components();
    }

    /**
     * Metodo para inicializar os componentes
     */
    public void components() {
        webFragment = WebFragment.newInstance("");
        navigationView = findViewById(R.id.navigationView);
        mainBinding.navigationView.setOnNavigationItemSelectedListener(this);
        fragments();
    }

    /**
     * Metodo para publicar APK no AppCenter para test
     */
    public void appCenter() {
        AppCenter.start(getApplication(), BuildConfig.APP_CENTER_KEY,
                Analytics.class, Crashes.class);
    }

    /**
     * Metodo para amazenar os favoritos usando
     */
    public void favorites(){
        SharedPreferences favorites = this.getSharedPreferences(
                getString(R.string.preference_favorites), Context.MODE_PRIVATE);
        SharedPreferences.Editor favEditor = favorites.edit();
        System.out.println("FAVORITES");
        System.out.println(favorites.getAll());
    }

    /**
     * Metodo para iniciar Fragmento Home
     */
    public void fragments() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.container, MainFragment.newInstance(), "main");
        ft.commit();
    }

    /**
     * Metodo para iniciar o Fragmento
     */
    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, "dashboard");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Metodo para navegar no BottomNavigation
     */
    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, MainFragment.newInstance())
                        .commit();
                break;

            case R.id.navigation_favorite:
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, FavoriteFragment.newInstance())
                        .commit();
                break;

            case R.id.navigation_help:
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, MenuOnFragment.newInstance())
                        .commit();
                break;
        }
        return true;
    }

    /**
     * Metodo para Ajustar o size do dispositivo
     */
    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }
}