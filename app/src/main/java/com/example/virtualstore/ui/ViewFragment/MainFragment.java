package com.example.virtualstore.ui.ViewFragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualstore.BuildConfig;
import com.example.virtualstore.R;
import com.example.virtualstore.adapter.ServiceAdapter;
import com.example.virtualstore.controller.LoadDialog;
import com.example.virtualstore.controller.Tracker;
import com.example.virtualstore.databinding.FragmentMainBinding;
import com.example.virtualstore.services.DataBase.ServicesDataSource;
import com.example.virtualstore.services.model.Company;
import com.example.virtualstore.services.model.Services;
import com.example.virtualstore.ui.ViewActivity.MainActivity;
import com.microsoft.appcenter.crashes.Crashes;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.navigation.fragment.NavHostFragment.findNavController;
import static com.example.virtualstore.controller.Tracker.StoreType.STORE_CATEGORY;
import static com.example.virtualstore.controller.Tracker.TrackType.DONE;
import static com.example.virtualstore.controller.Tracker.TrackType.FAILED;
import static com.example.virtualstore.controller.Tracker.TrackType.TRY;

public class MainFragment extends Fragment implements View.OnClickListener {

    private FragmentMainBinding mainBinding;
    private MainActivity main;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Services> servicesArrayList = new ArrayList<>();

    private ServicesDataSource mServicesDataSource;

    private LoadDialog loadDialogService;

    private Toast toast;
    private long lastBackPressTime = 0;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);

        components();

        return mainBinding.getRoot();
    }

    /**
     * Metodo para inicializar os componentes
     */
    public void components() {
        main = ((MainActivity) getActivity());
        loadDialogService = new LoadDialog(getContext());
        mainBinding.btnTryAgain.setOnClickListener(this);
        mainBinding.imgLogo.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_menuFragment));
        mainBinding.imgShare.setOnClickListener(this);
        setAPI();
        setmRecyclerView();
        onBackPressedFragment();
        getServicesApi();
    }

    /**
     * Metodo para Click
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnTryAgain) {
            getServicesApi();
        }
        if (view.getId() == R.id.imgShare) {
            getShare();
        }
    }

    /**
     * Metodo para iniciar as API's
     */
    public void setAPI() {
        mServicesDataSource = ServicesDataSource.getInstance();
    }

    /**
     * Metodo para setar o RecyclerView
     */
    public void setmRecyclerView() {
        mainBinding.recyclerService.setHasFixedSize(true);
        int numberOfColumns = 3;
        mLayoutManager = new GridLayoutManager(getContext(), numberOfColumns);
        mAdapter = new ServiceAdapter(getContext(), servicesArrayList, new ServiceAdapter.OnClick() {
            @Override
            public void selectServices(Services services, int position) {
                Log.d("Click on service", services.toString());
                servicesArrayList.clear();
                Company.id = services.getId();
                Bundle bundle = new Bundle();
                bundle.putString("header", services.getName());
                Navigation.findNavController(getView()).navigate(R.id.action_mainFragment_to_storeWebFragment, bundle);
//                getActivity().getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.container, StoreWebFragment.newInstance(services.getName()))
//                        .commit();
            }
        });

        mainBinding.recyclerService.setLayoutManager(mLayoutManager);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animationController = AnimationUtils.loadLayoutAnimation(getContext(), resId);
        mainBinding.recyclerService.setLayoutAnimation(animationController);

        mainBinding.recyclerService.setAdapter(mAdapter);
    }

    /**
     * Metodo para listar os serviços do back-end
     */
    public void getServicesApi() {
        LoadDialog.showStandardLoading(loadDialogService, getString(R.string.wait_searching_your_virtual_stores));

        mServicesDataSource.listServicesAll(new Callback<List<Services>>() {
            @Override
            public void onResponse(Call<List<Services>> call, Response<List<Services>> response) {
                System.out.println("LISTA DE SERVICOS COM SUCESSO");
                Log.d("SUCESSO", response.toString());
                Tracker.track(TRY, STORE_CATEGORY, response.toString());

                if (response.isSuccessful() && response.body() != null) {
                    Tracker.track(DONE, STORE_CATEGORY, response.toString());
                    servicesArrayList.clear();
                    servicesArrayList.addAll(response.body());
                    mAdapter.notifyDataSetChanged();
                    mainBinding.layoutListSErvice.setVisibility(View.VISIBLE);
                    mainBinding.LayoutErroService.setVisibility(View.GONE);
                    MainActivity.navigationView.setVisibility(View.VISIBLE);
                } else {
                    System.out.println("ERRO NA LISTAGEM DE SERVICOS");
                    Log.e("ERROR", response.toString());
                    Tracker.track(FAILED, STORE_CATEGORY, response.toString());

                    MainActivity.navigationView.setVisibility(View.GONE);
                    mainBinding.layoutListSErvice.setVisibility(View.GONE);
                    mainBinding.LayoutErroService.setVisibility(View.VISIBLE);
                }
                loadDialogService.dismissIt();
            }

            @Override
            public void onFailure(Call<List<Services>> call, Throwable t) {
                Tracker.track(FAILED, STORE_CATEGORY, t.getMessage());
                Crashes.trackError(t);

                System.out.println("ERRO NA REQUISICAO DOS SERVICOS");
                Log.e("ERROR-BACK-END", t.getMessage());

                MainActivity.navigationView.setVisibility(View.GONE);
                mainBinding.layoutListSErvice.setVisibility(View.GONE);
                mainBinding.LayoutErroService.setVisibility(View.VISIBLE);
                loadDialogService.dismissIt();
            }
        });
    }

    /**
     * Metodo para compartilhar o app
     */
    public void getShare() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
            String shareMesseger = getString(R.string.recommend_application);
            shareMesseger = shareMesseger + "\n\nhttps://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMesseger);
            startActivity(Intent.createChooser(shareIntent, getString(R.string.choose_one)));
        } catch (Exception e) {
            e.toString();
        }
    }

    /**
     * Metodo para instanciar o fragmento
     *
     * @return
     */
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    /*Metodo para o backSpace do celular*/
    private void onBackPressedFragment() {
        mainBinding.getRoot().setFocusableInTouchMode(true);
        mainBinding.getRoot().requestFocus();
        mainBinding.getRoot().setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                goToHome();
                return true;
            }
            return false;
        });
    }

    /*Metodo para voltar*/
    private void goToHome() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(getContext(), R.string.double_click_to_close_the_application, Toast.LENGTH_SHORT);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();
            }
//            super.getActivity().onBackPressed();
            getActivity().finish();
        }
    }
}
