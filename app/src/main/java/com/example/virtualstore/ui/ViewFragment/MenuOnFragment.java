package com.example.virtualstore.ui.ViewFragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.example.virtualstore.BuildConfig;
import com.example.virtualstore.R;
import com.example.virtualstore.controller.LoadDialog;
import com.example.virtualstore.databinding.FragmentMenuOnBinding;
import com.example.virtualstore.ui.ViewActivity.MainActivity;

public class MenuOnFragment extends Fragment {

    private FragmentMenuOnBinding menuOnBinding;
    private MainActivity main;

    public MenuOnFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        menuOnBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu_on, container, false);

        components();

        return menuOnBinding.getRoot();
    }

    /**
     * Metodo para inicializar os componentes
     */
    public void components() {
        main = ((MainActivity) getActivity());
        menuOnBinding.versionOn.setText("Versão " + BuildConfig.VERSION_NAME);
        MainActivity.navigationView.setVisibility(View.GONE);
        onBackPressedFragment();
    }


    /**
     * Metodo para instanciar o fragmento
     * @return
     */
    public static MenuOnFragment newInstance() {
        return new MenuOnFragment();
    }

    /*Metodo para o backSpace do celular*/
    private void onBackPressedFragment() {
        menuOnBinding.getRoot().setFocusableInTouchMode(true);
        menuOnBinding.getRoot().requestFocus();
        menuOnBinding.getRoot().setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                goToHome();
                return true;
            }
            return false;
        });
    }

    /*Metodo para voltar*/
    private void goToHome() {
        Fragment fragment = new MainFragment();
        main.openFragment(fragment);
//        Navigation.findNavController(getView()).popBackStack();
        MainActivity.navigationView.setVisibility(View.VISIBLE);
        MainActivity.navigationView.getMenu().getItem(0).setChecked(true);
//        menu.performIdentifierAction(R.id.navigation_home, 0);

    }
}