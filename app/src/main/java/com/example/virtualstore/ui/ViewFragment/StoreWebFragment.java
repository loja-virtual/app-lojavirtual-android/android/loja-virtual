package com.example.virtualstore.ui.ViewFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualstore.R;
import com.example.virtualstore.adapter.StoreWebAdapter;
import com.example.virtualstore.controller.LoadDialog;
import com.example.virtualstore.controller.Tracker;
import com.example.virtualstore.databinding.FragmentWebStoreBinding;
import com.example.virtualstore.services.DataBase.ServiceCompanyDataSource;
import com.example.virtualstore.services.model.Company;
import com.example.virtualstore.services.model.StoreWeb;
import com.example.virtualstore.ui.ViewActivity.MainActivity;
import com.microsoft.appcenter.crashes.Crashes;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.virtualstore.controller.Tracker.StoreType.STORE_COMPANY;
import static com.example.virtualstore.controller.Tracker.TrackType.DONE;
import static com.example.virtualstore.controller.Tracker.TrackType.FAILED;
import static com.example.virtualstore.controller.Tracker.TrackType.TRY;

public class StoreWebFragment extends Fragment implements View.OnClickListener {

    private FragmentWebStoreBinding webStoreBinding;
    private MainActivity main;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<StoreWeb> storesListWeb = new ArrayList<>();

    private ServiceCompanyDataSource mServiceCompanyDataSource;

    private LoadDialog loadDialogServiceStore;

    public StoreWebFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        webStoreBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_web_store, container, false);

        components();

        return webStoreBinding.getRoot();
    }

    /**
     * Metodo para inicializar os componentes
     */
    public void components() {
        main = ((MainActivity) getActivity());
        loadDialogServiceStore = new LoadDialog(getContext());
        webStoreBinding.btnBackHome.setOnClickListener(this);
        webStoreBinding.btnCompanyTryAgain.setOnClickListener(this);
        webStoreBinding.btnCompanyBack.setOnClickListener(this);
        String header = getArguments().getString("header");
        webStoreBinding.textHeader.setText(header);
        setAPI();
        setmRecyclerView();
        onBackPressedFragment();
        getServiceCompanyApi();
    }

    /**
     * Metodo para click
     */
    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_backHome:
            case R.id.btnCompanyBack:
                goToHome();
                break;

            case R.id.btnCompanyTryAgain:
                getServiceCompanyApi();
                break;
        }
    }

    /**
     * Metodo para iniciar as API's
     */
    public void setAPI() {
        mServiceCompanyDataSource = ServiceCompanyDataSource.getInstance();
    }

    /**
     * Metodo para setar o RecyclerView
     */
    public void setmRecyclerView() {
        webStoreBinding.recyclerHome.setHasFixedSize(true);
        int numberOfColumns = 2;
        mLayoutManager = new GridLayoutManager(getContext(), numberOfColumns);
        mAdapter = new StoreWebAdapter(getContext(), storesListWeb, new StoreWebAdapter.OnClick() {
            @Override
            public void selectWebStore(StoreWeb storeWeb, int position) {
                Log.d("Click on StoreWeb", storeWeb.toString());
                storesListWeb.clear();
                Bundle bundle = new Bundle();
                bundle.putString("key_url", storeWeb.getCompany().getLink());
                Navigation.findNavController(getView()).navigate(R.id.action_storeWebFragment_to_webFragment, bundle);
                    MainActivity.navigationView.setVisibility(View.GONE);
            }
        });

        webStoreBinding.recyclerHome.setLayoutManager(mLayoutManager);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animationController = AnimationUtils.loadLayoutAnimation(getContext(), resId);
        webStoreBinding.recyclerHome.setLayoutAnimation(animationController);

        webStoreBinding.recyclerHome.setAdapter(mAdapter);
    }

    /**
     * Metodo para listar as companhias do back-end
     */
    public void getServiceCompanyApi() {
        LoadDialog.showStandardLoading(loadDialogServiceStore, getString(R.string.wait_searching_your_virtual_stores));
        mServiceCompanyDataSource.listServiceCompanyId(Company.id, new Callback<List<StoreWeb>>() {
            @Override
            public void onResponse(Call<List<StoreWeb>> call, Response<List<StoreWeb>> response) {
                System.out.println("LISTA DE COMPANY");
                Log.d("SUCESSO", "LISTA DE COMPANY " + response.toString());
                Tracker.track(TRY,STORE_COMPANY,response.toString());

                if (response.isSuccessful() && response.body() != null && !response.body().isEmpty()) {
                    Tracker.track(DONE,STORE_COMPANY,response.toString());
                    storesListWeb.clear();
                    storesListWeb.addAll(response.body());
                    mAdapter.notifyDataSetChanged();
                    webStoreBinding.layoutListCompany.setVisibility(View.VISIBLE);
                    webStoreBinding.layoutErroCompany.setVisibility(View.GONE);
                    webStoreBinding.layoutNoCompany.setVisibility(View.GONE);
                } else {
                    System.out.println("ERRO NA LISTAGEM DE COMPANY");
                    Log.e("ERROR", "ERRO NA LISTAGEM DE COMPANY" + response.body());
                    Tracker.track(FAILED,STORE_COMPANY,response.toString());

                    webStoreBinding.layoutListCompany.setVisibility(View.GONE);
                    webStoreBinding.layoutNoCompany.setVisibility(View.VISIBLE);
                    webStoreBinding.layoutErroCompany.setVisibility(View.GONE);
                }
                loadDialogServiceStore.dismissIt();
            }

            @Override
            public void onFailure(Call<List<StoreWeb>> call, Throwable t) {
                Tracker.track(FAILED,STORE_COMPANY,t.getMessage());
                Crashes.trackError(t);

                System.out.println("ERRO NA REQUISICAO DOS COMPANYS");
                Log.d("ERROR-BACK-END", t.getMessage());

                webStoreBinding.layoutListCompany.setVisibility(View.GONE);
                webStoreBinding.layoutErroCompany.setVisibility(View.VISIBLE);
                webStoreBinding.layoutNoCompany.setVisibility(View.GONE);
                loadDialogServiceStore.dismissIt();
            }
        });
        MainActivity.navigationView.setVisibility(View.GONE);
    }

    /**
     * Metodo para instaciar o fragmento
     * @param header
     * @return
     */
    public static StoreWebFragment newInstance(String header) {
        StoreWebFragment fragment = new StoreWebFragment();
        Bundle bundle = new Bundle();
        bundle.putString("header", header);
        fragment.setArguments(bundle);
        return fragment;
    }

    /*Metodo para o backSpace do celular*/
    private void onBackPressedFragment() {
        webStoreBinding.getRoot().setFocusableInTouchMode(true);
        webStoreBinding.getRoot().requestFocus();
        webStoreBinding.getRoot().setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                goToHome();
                return true;
            }
            return false;
        });
    }

    /*Metodo para voltar*/
    private void goToHome() {
//        Fragment fragment = new MainFragment();
//        main.openFragment(fragment);
        Navigation.findNavController(getView()).popBackStack();
        MainActivity.navigationView.setVisibility(View.VISIBLE);
    }
}