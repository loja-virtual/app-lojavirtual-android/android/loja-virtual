package com.example.virtualstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.virtualstore.ui.ViewActivity.WelcomeActivity;

public class WelcomePagerAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    WelcomeActivity welcomeActivity;

     Context context;

    public WelcomePagerAdapter(){

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(position, container, false);
        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return welcomeActivity.layouts.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
