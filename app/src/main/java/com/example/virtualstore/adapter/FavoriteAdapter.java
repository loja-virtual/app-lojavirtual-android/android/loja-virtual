package com.example.virtualstore.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualstore.R;
import com.example.virtualstore.services.model.StoreWeb;
import com.example.virtualstore.util.ImgUtil;
import com.example.virtualstore.util.ScreenWidth;
import com.google.gson.Gson;

import java.util.ArrayList;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteHolder> {

    private ArrayList<StoreWeb> mStoreWebFavArrayList;
    private final Context context;
    public OnClick listener;
    private SharedPreferences favorites;
    private SharedPreferences.Editor favEditor;

    public interface OnClick {
        void selectFavStore(StoreWeb storeWeb, int position);
    }

    public class FavoriteHolder extends RecyclerView.ViewHolder {
        public CardView cardStore;
        public ImageView mImageHome;
        public ImageView mImageHomeFav;
        public TextView mDescriptionHome;

        public FavoriteHolder(@NonNull View itemView) {
            super(itemView);
            cardStore = itemView.findViewById(R.id.cardLayoutStore);
            mImageHome = itemView.findViewById(R.id.imgHome);
            mImageHomeFav = itemView.findViewById(R.id.icon_favorite);
            mDescriptionHome = itemView.findViewById(R.id.textCaschBack);
        }

        public void bindClick(StoreWeb storeWeb, int position, OnClick listener) {
            itemView.setOnClickListener(view -> listener.selectFavStore(storeWeb, position));
        }
    }

    public FavoriteAdapter(Context c, ArrayList<StoreWeb> storeWebFavArrayList, OnClick _Click) {
        mStoreWebFavArrayList = storeWebFavArrayList;
        System.out.println("storeWebFavArrayList");
        System.out.println(mStoreWebFavArrayList);
        this.context = c;
        listener = _Click;
        favorites = c.getSharedPreferences(
                c.getString(R.string.preference_favorites), Context.MODE_PRIVATE);
        favEditor = favorites.edit();
    }

    @NonNull
    @Override
    public FavoriteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_web_store, parent, false);
        FavoriteHolder homeStoreHolder = new FavoriteHolder(view);
        return homeStoreHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteHolder holder, int position) {
        StoreWeb storeWeb = mStoreWebFavArrayList.get(position);
        System.out.println("favBind");
        System.out.println(storeWeb.getCompany());
        if (favorites.getString(storeWeb.getCompany().getName(), null) != null) {
            holder.mImageHomeFav.setImageResource(R.drawable.ic_fav_select);
        }
        ImgUtil.requestImgWeb(context, holder.mImageHome, storeWeb.getCompany().getImage());
        holder.mImageHomeFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFavorite = favorites.getString(storeWeb.getCompany().getName(), null) != null;
                if (isFavorite) {
                    //System.out.println("Favorite");
                    favEditor.remove(storeWeb.getCompany().getName());
                    favEditor.commit();
                    holder.mImageHomeFav.setImageResource(R.drawable.ic_fav);
                    Toast.makeText(context, R.string.removed_from_favorites, Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.mDescriptionHome.setText(storeWeb.getCompany().getName());
        holder.cardStore.getLayoutParams().width = (int) (ScreenWidth.getScreenWidth() / 2.3);
        holder.bindClick(storeWeb, position, listener);
    }

    @Override
    public int getItemCount() {
        return mStoreWebFavArrayList.size();
    }
}