package com.example.virtualstore.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualstore.R;
import com.example.virtualstore.services.model.StoreWeb;
import com.example.virtualstore.util.ImgUtil;
import com.example.virtualstore.util.ScreenWidth;
import com.google.gson.Gson;

import java.util.ArrayList;

public class StoreWebAdapter extends RecyclerView.Adapter<StoreWebAdapter.HomeStoreHolder> {

    private ArrayList<StoreWeb> mStoreWebArrayList;
    private final Context context;
    public OnClick listener;
    private SharedPreferences favorites;
    private SharedPreferences.Editor favEditor;

    public interface OnClick {
        void selectWebStore(StoreWeb storeWeb, int position);
    }

    public class HomeStoreHolder extends RecyclerView.ViewHolder {
        public CardView cardStore;
        public ImageView mImageHome;
        public ImageView mImageHomeFav;
        public TextView mDescriptionHome;

        public HomeStoreHolder(@NonNull View itemView) {
            super(itemView);
            cardStore = itemView.findViewById(R.id.cardLayoutStore);
            mImageHome = itemView.findViewById(R.id.imgHome);
            mImageHomeFav = itemView.findViewById(R.id.icon_favorite);
            mDescriptionHome = itemView.findViewById(R.id.textCaschBack);

        }

        public void bindClick(StoreWeb storeWeb, int position, OnClick listener) {
            itemView.setOnClickListener(view -> listener.selectWebStore(storeWeb, position));
        }
    }

    public StoreWebAdapter(Context c, ArrayList<StoreWeb> storeWebArrayList, OnClick _Click) {
        mStoreWebArrayList = storeWebArrayList;
        this.context = c;
        listener = _Click;
        favorites = c.getSharedPreferences(
                c.getString(R.string.preference_favorites), Context.MODE_PRIVATE);
        favEditor = favorites.edit();

    }

    @NonNull
    @Override
    public HomeStoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_web_store, parent, false);
        HomeStoreHolder homeStoreHolder = new HomeStoreHolder(view);
        return homeStoreHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeStoreHolder holder, int position) {
        StoreWeb storeWeb = mStoreWebArrayList.get(position);
        if (favorites.getString(storeWeb.getCompany().getName(), null) != null) {
            holder.mImageHomeFav.setImageResource(R.drawable.ic_fav_select);
        }
        ImgUtil.requestImgWeb(context, holder.mImageHome, storeWeb.getCompany().getImage());
        holder.mImageHomeFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFavorite = favorites.getString(storeWeb.getCompany().getName(), null) != null;
                if (isFavorite) {
                    //System.out.println("Favorite");
                    favEditor.remove(storeWeb.getCompany().getName());
                    favEditor.commit();
                    holder.mImageHomeFav.setImageResource(R.drawable.ic_fav);
                    Toast.makeText(context, R.string.removed_from_favorites, Toast.LENGTH_SHORT).show();
                } else {
                    //System.out.println("NOT Favorite");
                    //System.out.println(storeWeb);
                    Gson gson = new Gson();
                    String json = gson.toJson(storeWeb);
                    favEditor.putString(storeWeb.getCompany().getName(), json);
                    favEditor.commit();
                    holder.mImageHomeFav.setImageResource(R.drawable.ic_fav_select);
                    Toast.makeText(context, R.string.added_to_favorites, Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.mDescriptionHome.setText(storeWeb.getCompany().getName());
        holder.cardStore.getLayoutParams().width = (int) (ScreenWidth.getScreenWidth() / 2.3);
        holder.bindClick(storeWeb, position, listener);
    }

    @Override
    public int getItemCount() {
        return mStoreWebArrayList.size();
    }
}