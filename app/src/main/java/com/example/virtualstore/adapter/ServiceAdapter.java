package com.example.virtualstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.virtualstore.R;
import com.example.virtualstore.services.model.Services;
import com.example.virtualstore.util.ImgUtil;
import com.example.virtualstore.util.ScreenWidth;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceHolder> {

    private ArrayList<Services> mServicesArrayList;
    private final Context context;
    public OnClick listener;

    public interface OnClick {
        void selectServices(Services services, int position);
    }

    public class ServiceHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout cardService;
        public ImageView mImageService;
        public TextView mNameService;

        public ServiceHolder(@NonNull View itemView) {
            super(itemView);
            cardService = itemView.findViewById(R.id.cardLayout);
            mImageService = itemView.findViewById(R.id.imageService);
            mNameService = itemView.findViewById(R.id.textNameService);
        }

        public void bindClick(Services services, int position, OnClick listener) {
            itemView.setOnClickListener(view -> listener.selectServices(services, position));
        }
    }

    public ServiceAdapter(Context c, ArrayList<Services> servicesArrayList, OnClick _Click) {
        mServicesArrayList = servicesArrayList;
        this.context = c;
        listener = _Click;
    }

    @NonNull
    @Override
    public ServiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_services, parent, false);
        ServiceHolder serviceHolder = new ServiceHolder(view);
        return serviceHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHolder holder, int position) {
        Services services = mServicesArrayList.get(position);
        ImgUtil.requestImg(context, holder.mImageService, services.getImage());
        holder.mNameService.setText(services.getName());
        holder.cardService.getLayoutParams().width = (int) (ScreenWidth.getScreenWidth() / 3.5);
        holder.bindClick(services, position, listener);
    }

    @Override
    public int getItemCount() {
        return mServicesArrayList.size();
    }
}