package com.example.virtualstore.util;

import android.content.res.Resources;

public class ScreenWidth {
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }
}
