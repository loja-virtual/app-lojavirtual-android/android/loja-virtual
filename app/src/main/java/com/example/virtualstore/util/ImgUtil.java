package com.example.virtualstore.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.virtualstore.BuildConfig;


public class ImgUtil {
    public static void requestImg(Context context, ImageView mImage, String imagePath) {
        GlideUrl url = new GlideUrl(imagePath);
        Glide.with(context).
                asDrawable().
                load(url).
                timeout(8000).
                centerCrop()
                .apply(new RequestOptions().override(200, 200))
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        mImage.setImageDrawable(resource);
                    }
                });
    }

    public static void requestImgWeb(Context context, ImageView mImage, String imagePath) {
        GlideUrl url = new GlideUrl(imagePath);
        Glide.with(context).
                asDrawable().
                load(url).
                timeout(8000).
                centerCrop()
                .apply(new RequestOptions().override(450, 200))
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        mImage.setImageDrawable(resource);
                    }
                });
    }
}
