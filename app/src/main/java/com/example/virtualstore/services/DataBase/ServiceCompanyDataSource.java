package com.example.virtualstore.services.DataBase;

import com.example.virtualstore.services.Api.ServiceCompanyController;
import com.example.virtualstore.services.model.StoreWeb;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ServiceCompanyDataSource extends RetrofitConfig {
    private static ServiceCompanyDataSource instance;
    private ServiceCompanyController api;

    public static ServiceCompanyDataSource getInstance() {
        if (instance == null) {
            instance = new ServiceCompanyDataSource();
        }
        return instance;
    }

    public ServiceCompanyDataSource() {
        setApi();
    }

    public void setApi() {
        api = retrofit.create(ServiceCompanyController.class);
    }

    public void listServiceCompanyAll(Callback<List<StoreWeb>> callback){
        Call<List<StoreWeb>> call = api.getServiceCompanyAll();
        call.enqueue(callback);
    }

    public void listServiceCompanyId(int id, Callback<List<StoreWeb>> callback){
        Call<List<StoreWeb>> call = api.getServiceCompanyId(id);
        call.enqueue(callback);
    }
}