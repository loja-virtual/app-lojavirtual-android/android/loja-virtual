package com.example.virtualstore.services.Api;

import com.example.virtualstore.services.model.StoreWeb;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceCompanyController {
    @GET("/serviceCompany")
    Call<List<StoreWeb>> getServiceCompanyAll();

    @GET("/serviceCompany/service/{id}")
    Call<List<StoreWeb>> getServiceCompanyId(
            @Path("id")Integer id
    );
}
