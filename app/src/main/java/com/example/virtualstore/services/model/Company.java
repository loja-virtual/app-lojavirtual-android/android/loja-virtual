package com.example.virtualstore.services.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Company {
    public static int id;
    private String image;
    private String name;
    private String link;
}
