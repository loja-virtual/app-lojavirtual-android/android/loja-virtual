package com.example.virtualstore.services.WebView;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.virtualstore.controller.LoadDialog;

public class WebViewClientImpl extends WebViewClient {

    private Activity activity;
    private LoadDialog loadDialog;

    public WebViewClientImpl(Activity activity, LoadDialog loadDialog) {
        this.activity = activity;
        this.loadDialog = loadDialog;
        LoadDialog.showStandardLoading(loadDialog,"Aguarde!");
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {

        if (url.contains("google.com.br")) return false;

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(intent);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        loadDialog.dismissIt();
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        loadDialog.dismissIt();
    }
}
