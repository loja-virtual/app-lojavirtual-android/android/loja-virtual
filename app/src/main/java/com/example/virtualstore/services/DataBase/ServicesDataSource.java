package com.example.virtualstore.services.DataBase;

import com.example.virtualstore.services.Api.ServiceController;
import com.example.virtualstore.services.model.Services;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ServicesDataSource extends RetrofitConfig {
    private static ServicesDataSource instance;
    private ServiceController api;

    public static ServicesDataSource getInstance() {
        if (instance == null) {
            instance = new ServicesDataSource();
        }
        return instance;
    }

    public ServicesDataSource() {
        setApi();
    }


    public void setApi() {
        api = retrofit.create(ServiceController.class);
    }

    public void listServicesAll(Callback<List<Services>> callback){
        Call<List<Services>> call = api.getServicesAll();
        call.enqueue(callback);
    }

    public void listServicesId(int id, Callback<Services> callback){
        Call<Services> call = api.getServicesId(id);
        call.enqueue(callback);
    }
}
