package com.example.virtualstore.services.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreWeb {
    private int id;
    private Company company;
}
