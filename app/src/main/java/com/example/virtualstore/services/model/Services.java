package com.example.virtualstore.services.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Services {
    private int id;
    private String image;
    private String name;
}
