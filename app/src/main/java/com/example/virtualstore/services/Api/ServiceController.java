package com.example.virtualstore.services.Api;

import com.example.virtualstore.services.model.Services;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceController {

    @GET("/service")
    Call<List<Services>> getServicesAll();

    @GET("/service/{id}")
    Call<Services> getServicesId(
            @Path("id") Integer id);
}
