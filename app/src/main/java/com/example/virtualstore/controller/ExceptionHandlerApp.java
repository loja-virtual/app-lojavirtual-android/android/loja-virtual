package com.example.virtualstore.controller;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.example.virtualstore.BuildConfig;
import com.microsoft.appcenter.crashes.Crashes;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandlerApp  implements Thread.UncaughtExceptionHandler{

    private final Context mContext;
    private final Class<?> mActivityClass;

    public ExceptionHandlerApp(Context context, Class<?> activityClass) {
        this.mContext = context;
        this.mActivityClass = activityClass;
    }

    @Override
    public void uncaughtException(@NonNull Thread t, @NonNull Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);

        Intent intent = new Intent(mContext, mActivityClass);
        String s = stackTrace.toString();

        if (BuildConfig.ANALYTICS){
            Crashes.trackError(exception);
        }
    }
}
