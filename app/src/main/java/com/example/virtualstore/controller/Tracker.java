package com.example.virtualstore.controller;

import com.example.virtualstore.BuildConfig;
import com.microsoft.appcenter.analytics.Analytics;

import java.util.HashMap;

public class Tracker {

    final static String TRACKER = "StoreTrack";

    public enum TrackType {
        TRY,
        DONE,
        FAILED
    }

    public enum StoreType {
        STORE_CATEGORY,
        STORE_COMPANY,
        STORE_WEB
    }

    public static void track(TrackType type, StoreType storeType, String extraInfo){
        if (BuildConfig.ANALYTICS){
            HashMap params = new HashMap();
            params.put(type.name(), storeType.name());
            params.put("EXTRA_INFOR", extraInfo);

            Analytics.trackEvent(storeType.name(),params);
        }
    }
}
