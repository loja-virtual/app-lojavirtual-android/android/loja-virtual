package com.example.virtualstore.controller;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class Permission extends Activity {
    String[] appPermissions = {
            Manifest.permission.INTERNET
    };

    public static final int CODE_PERMISSION_REQUIRED = 0;

    public boolean verifyPermissoes() {
        List<String> permissionRequired = new ArrayList<>();

        for (String permission : appPermissions) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                permissionRequired.add(permission);
            }
        }
        if (!permissionRequired.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionRequired.toArray(
                    new String[permissionRequired.size()]),CODE_PERMISSION_REQUIRED);
            return false;
        }
        return true;
    }
}
