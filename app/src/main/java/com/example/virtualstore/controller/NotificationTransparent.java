package com.example.virtualstore.controller;

import android.graphics.Color;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

import com.example.virtualstore.ui.ViewActivity.MainActivity;

public class NotificationTransparent {

    private MainActivity main = new MainActivity();

    /**
     * Tornando a barra de notificação transparente
     */
    public void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = main.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
