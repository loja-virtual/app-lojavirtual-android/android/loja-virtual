# Loja-Virtual
<p align="center">
    <img src="./img/ico_default.png" align="center" width="100px"/>
</p>

## Screenshots App Mobile
<p align="center">
   <img src="./img/loja_0.jpg" align="center" width="100px"/>
   <img src="./img/loja_1.jpg" align="center" width="100px"/>
   <img src="./img/loja_2.jpg" align="center" width="100px"/>
</p>
<p align="center">
   <img src="./img/loja_3.jpg" align="center" width="100px"/>
   <img src="./img/loja_4.jpg" align="center" width="100px"/>
   <img src="./img/loja_5.jpg" align="center" width="100px"/>
</p>

## Prévia da Aplicação
<p align="center">
   <img src="./img/loja_gif.gif" align="center" width="100px"/>
</p>

## Sobre
- Aplicação desenvolvida em Android Nativo para play-story utilizando uma requisição api on-line no heroku
- App lançado na Loja da Google 
- https://play.google.com/store/apps/details?id=com.softtechnology.virtualstore

## Tecnologias utilizadas
- Android Nativo
- Linguagem Java
- Arquitetura MVC
- Navigation
- Material Design
- Dialog Material
- LifeCycle - View-Model
- Lombok
- Retrofit
- Glide
- Firebase
- Biblioteca para Notificações do Firebase
- Biblioteca para dimensões SD/DP
- Biblioteca para imagem Lottie

## Requisitos
Para executar o projeto no dispositivo atravéis da IDE Android Studio gere o apk ou Run
- Build -> Build Bundle(s) -> Build APKs 
- Run App ou Shift + F10

## Executando o projeto
### Clonando o projeto
```bash
$ https://gitlab.com/loja-virtual/app-lojavirtual-android/android/loja-virtual.git
```
